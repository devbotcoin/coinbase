const objectUtil = require('../utils/object_util');
const ema = require('../indicators/custom_ema');
const bollingBand = require('../indicators/custom_bb');
const sma = require('../indicators/custom_sma');

class CustomSymbol {

    constructor(name, timeFrame, candlesInfos) {
        this.name = name;
        this.candlesInfos = {};
        this.candlesInfos[timeFrame] = candlesInfos;
    }

    initIndicatorIndex(timeFrame = "5m", indicators = {}) {
        if (objectUtil.isEmpty(indicators)) {
            return;
        }

        let candleData = this.candlesInfos[timeFrame];
        if (objectUtil.isEmpty(candleData)) {
            return;
        }



        // EMA
        let emaConfig = indicators["ema"];
        if (!objectUtil.isEmpty(emaConfig)) {
            ema.getEMAByPeriods(emaConfig, candleData);
        }

        //Bollinger band
        let bbConfig = indicators["bb"];
        if (!objectUtil.isEmpty(bbConfig)) {
            bollingBand.getBBByOptions(bbConfig, candleData);
        }

        //SMA volume
        let smaVolumeConfig = indicators["smaVolume"];
        if (!objectUtil.isEmpty(smaVolumeConfig)) {
            sma.getSmaVolumeByPeriod(smaVolumeConfig, candleData);
        }
    }

    mergeIndicatorIndexWithNewCandle(timeFrame = "5m", indicators = {}, currentCandle = {}) {

        if (objectUtil.isEmpty(indicators)) {
            return;
        }

        if (objectUtil.isEmpty(currentCandle)) {
            return;
        }

        let candleData = this.candlesInfos[timeFrame];
        if (objectUtil.isEmpty(candleData)) {
            return;
        }

        //remove last candle is not final
        while (!candleData[candleData.length - 1]["isFinal"]) {
            candleData.pop();
        }

        let prevCloseTimes = [];
        candleData.forEach(candle => {
            //time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored
            prevCloseTimes.push(candle["close"]);
        });

        // EMA
        let emaConfig = indicators["ema"];
        if (!objectUtil.isEmpty(emaConfig)) {
            let emaPeriods = emaConfig["periods"];
            emaPeriods.forEach(emaPeriod => {
                ema.initNewEmaByPeriod(emaPeriod, currentCandle, prevCloseTimes)
            });
        }

        //Bollinger band
        let bbConfig = indicators["bb"];
        if (!objectUtil.isEmpty(bbConfig)) {
            bollingBand.initNewBBByPeriod(bbConfig, currentCandle, prevCloseTimes);
        }

        //SMA volume
        let smaVolumeConfig = indicators["smaVolume"];
        if (!objectUtil.isEmpty(smaVolumeConfig)) {
            let prevVolumes = [];
            candleData.forEach(candle => {
                prevVolumes.push(candle["volume"]);
            });

            sma.initNewSmaVolumeByPeriod(smaVolumeConfig, currentCandle, prevVolumes);
        }

        //add current candle to list
        candleData.push(currentCandle);
        // console.log(currentCandle["smaVolume"]);
        // console.log(currentCandle["volume"]);
        // console.log("");
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    setCandlesInfos(candlesInfos) {
        this.candlesInfos = candlesInfos;
    }

    getCandlesInfos() {
        return this.candlesInfos;
    }
}

module.exports = CustomSymbol;