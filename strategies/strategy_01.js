/**
 *  Mua khi trend tăng
 *  Sử dụng đường trung bình cộng lũy thừa (EMA) + BB để xác định xu hướng + bắt đáy.
 *  EMA : vẽ các đường ema 9, 20, 25, 30, 35, 40, 45, 50, 55, 60, 69
 *  Sử dụng cây nến ở 1h trước, sau đó dùng cây nến 30p, 15p để confirm lại
 *  @Notice : Trừ điều kiện EMA, còn lại phải thỏa mãn : Khối lương trung bình >= (trung bình 4 khung trước ) * 1.0, alert luôn độ lệch của volume với MA20
 *  Điểm nên mua :
 *      + Khi nào ema 9 đi lên cắt ema 69
 *      + Giá cắt BB-
 *  Điểm nên bán :
 *      + Khi có 1 cây nến thân dài, volumn cao nhất : close - open >= 10%; nếu đi kèm với BB+ thì alert trong cùng 1 message (1)
 *      + Hoặc cây nến phía sau là nến đỏ + thỏa mãn điều kiện 1
 *      + Ema9 cắt xuống ema69
 *  Alert các dấu hiệu :
 *      + Khi giá cắt BB+
 */
'use strict';

const sma = require('technicalindicators').SMA;

const objectUtil = require('../utils/object_util');
const stringUtil = require('../utils/string_util');
const numberUtil = require('../utils/number');
const CustomSymbol = require('../models/custom_symbol');

const binanceSymbols = require('../exchange_infos/binance/binance_symbol');
const DiscordNotification = require('../notifications/discord');
const discordChannels = {};

const emaPeriods = [9, 20, 25, 30, 35, 40, 45, 50, 55, 60, 69];

const avgTimeFrame = 4;
const VOLUME_MA = 20;
const strategyData = {
    "ema": {"periods": emaPeriods},
    "bb": {"period": 20, "stdDev": 2},
    "smaVolume": {"period": VOLUME_MA}
};

class Strategy01 {
    constructor(timeFrame, exchangeApi, prevDay) {
        this.timeFrame = timeFrame;
        this.exchangeApi = exchangeApi;
        this.prevDay = prevDay;
    }

    init() {
        console.log("Config webhook");

        let timeFrameChannelGeneral = this.timeFrame + "-general";
        let timeFrameChannelTrade = this.timeFrame + "-trade";

        let discordNotificationInstanceGeneral = new DiscordNotification(timeFrameChannelGeneral);
        discordChannels[timeFrameChannelGeneral] = discordNotificationInstanceGeneral;

        let discordNotificationInstanceTrade = new DiscordNotification(timeFrameChannelTrade);
        discordChannels[timeFrameChannelTrade] = discordNotificationInstanceTrade;

        return new Promise(resolve => {
            setTimeout(function () {
                discordChannels[timeFrameChannelTrade].alertSuccess("Started");
                discordChannels[timeFrameChannelGeneral].alertSuccess("Started");
                resolve();
            }, 10000)
        })
    }

    execute() {
        let self = this;
        try {
            binanceSymbols.forEach((symbolCode, index) => {
                    setTimeout(async function () {
                        let ticks = await
                            self.getPrevCandles(symbolCode, self.timeFrame);
                        //remove current tick
                        ticks.pop();

                        let prevCandles = [];
                        ticks.forEach(tick => {
                            let [time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored] = tick;
                            prevCandles.push({
                                "time": time,
                                "open": open,
                                "high": high,
                                "low": low,
                                "close": close,
                                "volume": volume,
                                "closeTime": closeTime,
                                "assetVolume": assetVolume,
                                "trades": trades,
                                "buyBaseVolume": buyBaseVolume,
                                "buyAssetVolume": buyAssetVolume,
                                "ignored": ignored,
                                "isFinal": true
                            });
                        });

                        let symbol = new CustomSymbol(symbolCode, self.timeFrame, prevCandles);
                        symbol.initIndicatorIndex(self.timeFrame, strategyData);
                        //tracking new candle
                        self.exchangeApi.getKlineStream({
                                symbol: symbol.getName(),
                                timeFrame: self.timeFrame
                            }, function (candlesticks) {
                                console.log(symbol.getName() + " is tracking in " + self.timeFrame);

                                let shortTick = candlesticks.k;

                                //normalize tick
                                //let { o:open, h:high, l:low, c:close, v:volume, n:trades, i:interval, x:isFinal, q:quoteVolume, V:buyVolume, Q:quoteBuyVolume } = ticks;
                                let currentTick = {
                                    "open": shortTick.o,
                                    "high": shortTick.h,
                                    "low": shortTick.l,
                                    "close": shortTick.c,
                                    "volume": shortTick.v,
                                    "isFinal": shortTick.x
                                };

                                symbol.mergeIndicatorIndexWithNewCandle(self.timeFrame, strategyData, currentTick);

                                let candlesInfos = symbol.getCandlesInfos()[self.timeFrame];

                                //tỉ lệ volume hiện tại với ma20
                                let ratioVolume = self.currentVolumeMoreThanAvgPrevVolumes(symbol.getName(), candlesInfos, currentTick["volume"], self.timeFrame, true);
                                //neu ket thuc cay nen hien tai

                                let prevDayInfo = self.prevDay.getChange24HInfo(symbol.getName());

                                let commonMsgs = [{
                                    "title": "Volume hiện tại",
                                    "value": numberUtil.convertFloat(currentTick["volume"]) + " tăng " + numberUtil.convertFloat(currentTick["volume"] / currentTick["smaVolume"]) + " lần"
                                }, {
                                    "title": "Giá hiện tại",
                                    "value": currentTick["close"]
                                }, {
                                    "title": "Giá 24hr low/high",
                                    "value": "**" + prevDayInfo["low"] + "**" + "/" + "**" + prevDayInfo["high"] + "**"
                                }, {
                                    "title": "24h change",
                                    "value": prevDayInfo["change"] + "%"
                                }, {
                                    "title": "Vốn hóa 24h",
                                    "value": "Xếp thứ " + prevDayInfo["rate"]
                                }]

                                if (currentTick.isFinal) {
                                    let resultEmaTracking = self.trackingEMA(candlesInfos);
                                    if (resultEmaTracking == 1) {


                                        let eMAPositiveMsgs = [{
                                            "title": "EMA +",
                                            "value": "Xem xét mua vào"
                                        }];
                                        eMAPositiveMsgs = eMAPositiveMsgs.concat(commonMsgs);
                                        discordChannels[self.timeFrame + "-general"].alertSuccessMsgs(stringUtil.nomalizeSymbol(symbol.getName()), eMAPositiveMsgs);

                                        discordChannels[self.timeFrame + "-trade"].alertSuccessMsgs(stringUtil.nomalizeSymbol(symbol.getName()), eMAPositiveMsgs);
                                    } else if (resultEmaTracking == 2) {
                                        let eMANegativeMsgs = [{
                                            "title": "EMA -",
                                            "value": "Xem xét bán ra"
                                        }];
                                        eMANegativeMsgs = eMANegativeMsgs.concat(commonMsgs);
                                        discordChannels[self.timeFrame + "-general"].alertErrorMsgs(stringUtil.nomalizeSymbol(symbol.getName()), eMANegativeMsgs);
                                        discordChannels[self.timeFrame + "-trade"].alertErrorMsgs(stringUtil.nomalizeSymbol(symbol.getName()), eMANegativeMsgs);
                                    }
                                }

                                let resultBBTracking = self.trackingBB(candlesInfos);
                                if (ratioVolume > 0) {
                                    // kiem tra gia close >= 10% gia mo cua
                                    let msgPriceMutation = [];
                                    if (Number(currentTick["close"]) / Number(currentTick["open"]) >= 1.1) {
                                        msgPriceMutation = [{
                                            "title": "Giá tăng đột biến - Xem xét bán",
                                            "value": Number(currentTick["close"]) / Number(currentTick["open"]) * 100 + " % so với giá mở cửa"
                                        }]
                                        commonMsgs = msgPriceMutation.concat(commonMsgs);
                                    }

                                    if (resultBBTracking["state"] == 1) {
                                        let bbPositiveMsgs = [{
                                            "title": "BB +",
                                            "value": "Giá vượt BB+ : " + resultBBTracking["ratio"] + "%"
                                        }];
                                        bbPositiveMsgs = bbPositiveMsgs.concat(commonMsgs);

                                        discordChannels[self.timeFrame + "-trade"].alertErrorMsgs(stringUtil.nomalizeSymbol(symbol.getName()), bbPositiveMsgs);
                                        discordChannels[self.timeFrame + "-general"].alertErrorMsgs(stringUtil.nomalizeSymbol(symbol.getName()), bbPositiveMsgs);
                                    } else if (resultBBTracking["state"] == 2) {

                                        let bbNegativeMsgs = [{
                                            "title": "BB -",
                                            "value": "Giá vượt BB- : " + resultBBTracking["ratio"] + "%"
                                        }];
                                        bbNegativeMsgs = bbNegativeMsgs.concat(commonMsgs);

                                        discordChannels[self.timeFrame + "-trade"].alertSuccessMsgs(stringUtil.nomalizeSymbol(symbol.getName()), bbNegativeMsgs);
                                        discordChannels[self.timeFrame + "-general"].alertSuccessMsgs(stringUtil.nomalizeSymbol(symbol.getName()), bbNegativeMsgs);
                                    }
                                } else {
                                    if (resultBBTracking["state"] == 1) {
                                        let msgOverBought = [];

                                        // Kiểm tra nến đỏ
                                        if (Number(currentTick["close"]) < Number(currentTick["open"])) {
                                            let prevTicks = candlesInfos.slice(0, candlesInfos.length - 1);
                                            let prevTick = prevTicks[prevTicks.length - 1];

                                            //Kiểm tra phía trước là nến xanh
                                            if (Number(prevTick["close"]) > Number(prevTick["open"])) {

                                                //Kiểm tra vượt BB+ và volume đột biến
                                                let ratioPrevVolume = self.currentVolumeMoreThanAvgPrevVolumes(symbol.getName(), prevTicks, prevTick["volume"], self.timeFrame, false);
                                                let resultPrevBBTracking = self.trackingBB(prevTicks);

                                                if (ratioPrevVolume > 0 && resultPrevBBTracking == 1) {
                                                    msgOverBought = [{
                                                        "title": "Nến hiện tại đỏ BB+, nến trước xanh - Xem xét bán",
                                                        "value": "Giá vượt BB+ : " + resultBBTracking["ratio"] + "%"
                                                    }]
                                                    msgOverBought = msgOverBought.concat(commonMsgs);

                                                    discordChannels[self.timeFrame + "-trade"].alertErrorMsgs(stringUtil.nomalizeSymbol(symbol.getName()), msgOverBought);
                                                    discordChannels[self.timeFrame + "-general"].alertErrorMsgs(stringUtil.nomalizeSymbol(symbol.getName()), msgOverBought);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        )
                    }, 5000 * index)
                }
            )
        } catch (error) {
            console.error(error);
        }
    }

    getPrevCandles(symbol, timeFrame) {
        let options = {
            symbol: symbol,
            interval: timeFrame,
            limit: 500,
            endTime: new Date().getTime()
        };
        return this.exchangeApi.getCandles(options);
    }

    currentVolumeMoreThanAvgPrevVolumes(symbol, candlestick = [], currentVolume, timeFrame, isAlert) {

        let countDown = avgTimeFrame + 1;
        let totalPrevVolumes = 0;
        while (countDown > 1) {
            totalPrevVolumes += Number(candlestick[candlestick.length - countDown]["volume"]);
            countDown--;
        }

        if (Number(currentVolume) >= (1.0 * totalPrevVolumes / avgTimeFrame)) {
            let currentPrice = candlestick[candlestick.length - 1]["close"];

            let lastSMAVolume = Number(candlestick[candlestick.length - 1]["smaVolume"]);

            let ratio = numberUtil.convertFloat(Number(currentVolume) / lastSMAVolume);

            if (ratio >= 1) {
                let lvlWarning = 0;
                if (1.25 <= ratio && ratio < 1.5) {
                    lvlWarning = 1;
                } else if (1.5 <= ratio && ratio < 2) {
                    lvlWarning = 2;
                } else if (ratio >= 2) {
                    lvlWarning = 3;
                }
                if (isAlert) {
                    let msgs = [{
                        "title": "Volume hiện tại",
                        "value": numberUtil.convertFloat(currentVolume) + " tăng " + ratio + " lần"
                    }, {
                        "title": "Giá hiện tại",
                        "value": currentPrice
                    }]
                    discordChannels[timeFrame + "-general"].alertWarningMsgs(stringUtil.nomalizeSymbol(symbol), msgs, ratio);
                    console.log(stringUtil.nomalizeSymbol(symbol) + " Volume : " + currentVolume + " tăng " + ratio + " lần");
                }
                return ratio;
            } else {
                // discordChannels[timeFrame + "-general"].alertNormal(" Volume : " + numberUtil.convertFloat(currentVolume) + " lớn hơn trung bình " + avgTimeFrame + " khung trước . Giá : " + currentPrice, stringUtil.nomalizeSymbol(symbol));
                // console.log(stringUtil.nomalizeSymbol(symbol) + " Volume : " + currentVolume + " lớn hơn trung bình " + avgTimeFrame + " khung trước");
            }
        }
        return -1;
    }

    trackingEMA(candlesInfos) {
        let startCandleInfo = candlesInfos[candlesInfos.length - 2];
        let lastCandleInfo = candlesInfos[candlesInfos.length - 1];

        if (objectUtil.isEmpty(startCandleInfo["emaPeriods"])) {
            console.log("ema not data");
            return 0;
        }

        // tracking EMA +
        if (startCandleInfo["emaPeriods"][9] <= startCandleInfo["emaPeriods"][69] && startCandleInfo["emaPeriods"][9] >= startCandleInfo["emaPeriods"][69]) {
            return 1;
        }

        //tracking EMA -
        if (startCandleInfo["emaPeriods"][9] >= startCandleInfo["emaPeriods"][69] && startCandleInfo["emaPeriods"][9] <= startCandleInfo["emaPeriods"][69]) {
            return 2;
        }

        return 0;
    }

    trackingBB(candlesInfos) {

        let lastCandleInfo = candlesInfos[candlesInfos.length - 1];

        //tracking BB +
        if (lastCandleInfo["bb"]["upper"] <= lastCandleInfo["high"]) {
            console.log("Giá hiện tại : " + lastCandleInfo["high"] + " BB (+)  : " + lastCandleInfo["bb"]["upper"]);
            return {
                "state": 1,
                "ratio": numberUtil.convertFloat(100 * (lastCandleInfo["high"] / lastCandleInfo["bb"]["upper"] - 1))
            };
        }

        //tracking BB -

        if (lastCandleInfo["bb"]["lower"] >= lastCandleInfo["low"]) {
            console.log("Giá hiện tại : " + lastCandleInfo["low"] + " BB (-)  : " + lastCandleInfo["bb"]["lower"]);
            return {
                "state": 2,
                "ratio": numberUtil.convertFloat(100 * (lastCandleInfo["bb"]["lower"] / lastCandleInfo["low"] - 1))
            };
            return 2;
        }

        return {
            "state": 0
        }
    }
};

module.exports = Strategy01;
