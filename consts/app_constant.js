let appConstant = {
    BINANCE : "binance",

    BINANCE_4H_GENERAL_WEBHOOK : "https://discordapp.com/api/webhooks/403968638111186957/-tdMzVm1RkEKdjx9yiIN6PZzAi65EaCMYtXsa3HC5Un3faccsxGDWsPH7mCuHqRZMN_R",
    BINANCE_4H_TRADE_WEBHOOK : "https://discordapp.com/api/webhooks/403968786270519296/WcpPGaOpmXBgtGKmjwPURd1zC4w0NVNfM1_RJBbPdL39eqq9P-tL5UaiJ7GEeyIW5POM",
    BINANCE_1H_GENERAL_WEBHOOK : "https://discordapp.com/api/webhooks/403968836375937024/OwSW1_zzRjYKnSHN4VmVGM5By465s7fbHr3UEzvfntcNg_GWfMDFruBSAxLyMRypp_hI",
    BINANCE_1H_TRADE_WEBHOOK : "https://discordapp.com/api/webhooks/403968865668825089/E2PunO3osMKSp5LLlM_PWneViHJ-vlGPGm98lyGaodP8vIsadA_P-MtTxRO7CGdORb-Y",
    BINANCE_15M_GENERAL_WEBHOOK : "https://discordapp.com/api/webhooks/403968899223388160/uO9SzpqQ1dqw7OulLwieLTsLdagUxRc36_6C3aEBiSsMvD_voUltlTtjj60SJNPaVsfg",
    BINANCE_15M_TRADE_WEBHOOK : "https://discordapp.com/api/webhooks/403968935545929730/kR3LnKyEWgYINAeFd1B-GEDGFK7mYkWyA5p_iTbZTXESaAqyX-c8AkPuSzTJ5rnJatgV",
}

module.exports = appConstant;