const request = require("request")
const endpoint = "https://discordapp.com/api/"
const appConst = require("../consts/app_constant");
const BOT_NAME = "SCORPION";

class DiscordNotification {

    constructor(timeFrameChannel) {
        this.webhook = {};
        console.log(timeFrameChannel);
        let webhookUrl = "";
        switch (timeFrameChannel) {
            case "4h-general" :
                webhookUrl = appConst.BINANCE_4H_GENERAL_WEBHOOK;
                break;
            case "4h-trade" :
                webhookUrl = appConst.BINANCE_4H_TRADE_WEBHOOK;
                break;
            case "1h-general" :
                webhookUrl = appConst.BINANCE_1H_GENERAL_WEBHOOK;
                break;
            case "1h-trade" :
                webhookUrl = appConst.BINANCE_1H_TRADE_WEBHOOK;
                break;
            case "15m-general" :
                webhookUrl = appConst.BINANCE_15M_GENERAL_WEBHOOK;
                break;
            case "15m-trade" :
                webhookUrl = appConst.BINANCE_15M_TRADE_WEBHOOK;
                break;
        }

        this.url = webhookUrl;
        this.id = ""
        this.token = "";
        let self = this;
        request(this.url, function (error, response, body) {
            if (error) {
                console.log("Could not get webhook info: " + error)
                return
            }
            try {
                this.rawData = JSON.parse(body)
                self.webhook.token = this.rawData.token
                self.webhook.id = this.rawData.id
                self.webhook.guild_id = this.rawData.guild_id
                self.webhook.channel_id = this.rawData.channel_id
                self.webhook.name = this.rawData.name
                self.webhook.avatar = this.rawData.avatar
                console.log("Created webhook with token: " + self.webhook.token + " and ID: " + self.webhook.id)

                return "Creating webhook"
            } catch (err) {
                console.log("Could not create webhook: " + err.stack)
            }
        })
    }


    alertWarning(msgContent, msgTitle, lvl) {
        console.log(lvl);
        switch (lvl) {
            case 0 :
                console.log("lvl0");
                this.alertInfo(msgContent, msgTitle);
                break;
            case 1 :
                console.log("lvl1");
                this.alertWarningLevel1(msgContent, msgTitle);
                break;
            case 2 :
                console.log("lvl2");
                this.alertWarningLevel2(msgContent, msgTitle);
                break;
            case 3 :
                console.log("lvl3");
                this.alertWarningLevel3(msgContent, msgTitle);
                break;
        }
    }

    alertSuccess(msgContent, msgTitle = "Success") {
        this.custom(BOT_NAME, msgContent, msgTitle, "#04ff00");
    }

    alertInfo(msgContent, msgTitle = "info") {
        this.custom(BOT_NAME, msgContent, msgTitle, "#00fffa");
    }

    alertWarningLevel1(msgContent, msgTitle = "Warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!") {
        this.custom(BOT_NAME, msgContent, msgTitle, "#fffb7a");
    }

    alertWarningLevel2(msgContent, msgTitle = "Warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!") {
        this.custom(BOT_NAME, msgContent, msgTitle, "#ffa346");
    }

    alertWarningLevel3(msgContent, msgTitle = "Warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!") {
        this.custom(BOT_NAME, msgContent, msgTitle, "#ff38a8");
    }

    alertError(msgContent, msgTitle = "Error") {
        this.custom(BOT_NAME, msgContent, msgTitle, "#ff0000");
    }

    alertNormal(msgContent, msgTitle, color = "#0101DF") {
        this.custom(BOT_NAME, msgContent, msgTitle, color);
    }

    alertSuccess(msgContent, msgTitle = "Success") {
        this.custom(BOT_NAME, msgContent, msgTitle, "#04ff00");
    }

    alertInfo(msgContent, msgTitle = "info") {
        this.custom(BOT_NAME, msgContent, msgTitle, "#00fffa");
    }

    //alert many msgs with format
    alertWarningMsgs(msgTitle, fields, lvl) {
        console.log(lvl);
        switch (lvl) {
            case 0 :
                console.log("lvl0");
                this.alertInfoMsgs(msgTitle, fields);
                break;
            case 1 :
                console.log("lvl1");
                this.alertWarningLevel1Msgs(msgTitle, fields);
                break;
            case 2 :
                console.log("lvl2");
                this.alertWarningLevel2Msgs(msgTitle, fields);
                break;
            case 3 :
                console.log("lvl3");
                this.alertWarningLevel3Msgs(msgTitle, fields);
                break;
        }
    }

    alertSuccessMsgs(msgTitle = "Success", fields) {
        this.customMsgs(BOT_NAME, msgTitle, fields, "#04ff00");
    }

    alertInfoMsgs(msgTitle = "info", fields) {
        this.customMsgs(BOT_NAME, msgTitle, fields, "#00fffa");
    }

    alertWarningLevel1Msgs(msgTitle, fields = "Warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!") {
        this.customMsgs(BOT_NAME, msgTitle, fields, "#fffb7a");
    }

    alertWarningLevel2Msgs(msgTitle, fields = "Warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!") {
        this.customMsgs(BOT_NAME, msgTitle, fields, "#ffa346");
    }

    alertWarningLevel3Msgs(msgTitle, fields = "Warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!") {
        this.customMsgs(BOT_NAME, msgTitle, fields, "#ff38a8");
    }

    alertErrorMsgs(msgTitle, fields = "Error") {
        this.customMsgs(BOT_NAME, msgTitle, fields, "#ff0000");
    }

    alertNormalMsgs(msgTitle, fields, color = "#0101DF") {
        this.customMsgs(BOT_NAME, msgTitle, fields, color);
    }

    custom(name = BOT_NAME, msg, title, color) {
        if (color) {
            var data = {
                "username": name,
                "text": "[]()",
                "attachments": [{
                    "color": color,
                    "fields": [{
                        "title": title,
                        "value": msg
                    }],
                    "ts": new Date() / 1000
                }]
            }
        } else {
            var data = {
                "username": name,
                "text": "[]()",
                "attachments": [{
                    "fields": [{
                        "title": title,
                        "value": msg
                    }],
                    "ts": new Date() / 1000
                }]
            }
        }
        let self = this;
        request({
            url: endpoint + "webhooks/" + self.webhook.id + "/" + self.webhook.token + "/slack",
            method: "POST",
            body: data,
            json: true
        }, (e, r, b) => {
            if (e) throw e;
        });

    }

    customMsgs(name = BOT_NAME, title, fields, color) {
        if (color) {
            var data = {
                "username": name,
                "text": "[]()",
                "attachments": [{
                    "title": title,
                    "color": color,
                    "fields": fields,
                    "ts": new Date() / 1000
                }]
            }
        } else {
            var data = {
                "username": name,
                "text": "[]()",
                "attachments": [{
                    "title": title,
                    "fields": fields,
                    "ts": new Date() / 1000
                }]
            }
        }
        let self = this;
        request({
            url: endpoint + "webhooks/" + self.webhook.id + "/" + self.webhook.token + "/slack",
            method: "POST",
            body: data,
            json: true
        }, (e, r, b) => {
            if (e) throw e;
        });

    }


}

module.exports = DiscordNotification;