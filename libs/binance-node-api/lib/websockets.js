const WebSocket = require('ws')
const axios = require('axios')
const Sockets = {}
const webSocketInstance = {};

const baseWSS = "wss://stream.binance.com:9443/ws"

let initSocket = function (path, eventHandler) {

    let ws = webSocketInstance[baseWSS + path];
    if (ws === undefined) {
        ws = new WebSocket(baseWSS + path);
        webSocketInstance[baseWSS + path] = ws;
    }

    ws.on('message', eventHandler)
}

// Get depth stream for pair
Sockets.getDepthStream = function (symbol, eventHandler) {
    initSocket(`/${symbol.toLowerCase()}@depth`, eventHandler)
}

// Get Kline data for a pair
Sockets.getKlineStream = function (symbol, interval, eventHandler) {
    initSocket(`/${symbol.toLowerCase()}@kline_${interval}`, eventHandler)
}

// Get Aggregate Trade info
Sockets.getAggTradeStream = function (symbol, eventHandler) {
    initSocket(`/${symbol.toLowerCase()}@aggTrade`, eventHandler)
}

// Get 24h stream
Sockets.getPrevDay = function (symbol, eventHandler) {
    let streamName = symbol ? symbol.toLowerCase() + '@ticker' : '!ticker@arr';
    initSocket(`/${streamName}`, eventHandler)
}

// [NEEDS FURTHER WORK]
// Get user data stream info 
// Sockets.getUserDataStream = function(listenKey, eventHandler) {
//   initSocket(`/${listenKey}`, eventHandler)
// }

// Get listenKey for maintaining a stream and accessing account info through web sockets
Sockets.startDataStream = function () {
    let url = this.v1URL + '/userDataStream'
    return axios.post(url, null, this.headers)
}

// Ping to keep data stream alive
Sockets.keepDataStream = function (listenKey) {
    let url = this.v1URL + '/userDataStream'
    return axios({
        method: 'put',
        url: url,
        data: this.formatQuery(listenKey).slice(1),
        headers: this.headers.headers
    })
}

// Close the data stream
Sockets.closeDataStream = function (listenKey) {
    let url = this.v1URL + '/userDataStream'
    return axios({
        method: 'delete',
        url: url,
        data: this.formatQuery(listenKey).slice(1),
        headers: this.headers
    })
}

module.exports = Sockets