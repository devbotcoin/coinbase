const binance = require('./binance-node-api/binance');
const appConstant = require('../consts/app_constant');

const ExchangeApi = {};

ExchangeApi.init = function (exchangeName, config) {
    this.exchangeName = exchangeName;
    switch (this.exchangeName) {
        case appConstant.BINANCE : {
            this.api = Object.create(binance);
            this.api.init(config);
            break;
        }
    }
}

ExchangeApi.getCandles = async function (options) {
    let ticks;
    try {
        switch (this.exchangeName) {
            case appConstant.BINANCE : {
                let response = await this.api.getCandles(options);
                return response.data;
            }
        }
    } catch (error) {
        console.error(error);
    }
}

ExchangeApi.getKlineStream = function (options, callback) {
    try {
        switch (this.exchangeName) {
            case appConstant.BINANCE : {
                this.api.getKlineStream(options.symbol, options.timeFrame, response => {
                    callback(JSON.parse(response));
                });
                break;
            }
        }
    } catch (error) {
        console.error(error);
    }
}

ExchangeApi.getPrevDay = function(symbols, callback) {
    try {
        switch (this.exchangeName) {
            case appConstant.BINANCE : {
                this.api.getPrevDay(symbols, response => {
                    callback(JSON.parse(response));
                });
                break;
            }
        }
    } catch (error) {
        console.error(error);
    }
}
module.exports = ExchangeApi;