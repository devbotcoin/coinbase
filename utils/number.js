const objectUtil = require('../utils/object_util');

NumberUtil = {};

NumberUtil.convertFloat = function (num) {
    if (num === undefined ) {
        num = 0;
    }
    return parseFloat(Number(num).toFixed(2)).toLocaleString();
}

module.exports = NumberUtil;

