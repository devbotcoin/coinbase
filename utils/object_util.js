const ObjectUtil = {};

ObjectUtil.isEmpty = function (obj) {
    if (!obj || obj === undefined || Object.keys(obj).length <= 0) {
        return true;
    }
    return false;
}

module.exports = ObjectUtil;

