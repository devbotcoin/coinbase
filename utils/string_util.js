StringUtil = {};

StringUtil.nomalizeSymbol = function (symbol) {
    symbol += "_"
    symbol = symbol.replace("BTC_", "/BTC");
    symbol = symbol.replace("USDT_", "/USDT");
    symbol = symbol.replace("BNB_", "/BNB");
    symbol = symbol.replace("ETH_", "/ETH");

    return symbol;
}

module.exports = StringUtil;