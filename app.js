var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var stylus = require('stylus');

var index = require('./routes/index');
var users = require('./routes/users');
const Strategy01 = require('./strategies/strategy_01');
const PrevDay = require('./exchange_infos/prev_day');

const exchangeApi = require('./libs/exchange_api');
const binanceConfig = {
    apiKey: 'cXDsVvPpjzNxMqPah4P9OH3h8xAoRKwPjxRnqTTEb6DUPPvUOppMFZGn5H7UocNN',
    secretKey: 'vn1zPvZZAbaumX5TpM0MUXfJL2I8pVxiRZivA2Su79NXfKC3MpIj6ByJXcoCtUGg'
};
exchangeApi.init("binance", binanceConfig);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

//create socket get symbol data change in 24hr
console.log("symbol data change in 24hr");
let prevDay = new PrevDay(exchangeApi);

setTimeout(function () {
    let timeFrames = ["15m", "1h", "4h"];

    timeFrames.forEach((timeFrame, index) => {
        setTimeout(async function () {

            let strategyInstance = new Strategy01(timeFrame, exchangeApi, prevDay);
            await strategyInstance.init();
            strategyInstance.execute();
        }, 60000 * index);
    })
}, 30000);


module.exports = app;
