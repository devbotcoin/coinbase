const bollBand = require('technicalindicators').BollingerBands;
const objectUtil = require('../utils/object_util');

const DEFAULT_PERIOD = 20;
const DEFAULT_STD_DEV = 2;

CustomBB = {};

CustomBB.getBBByOptions = function (bbConfig, candleData = []) {

    let closeTimes = [];
    candleData.forEach(candle => {
        closeTimes.push(candle["close"]);
    });

    //calculate bb
    candleData.forEach((candle, index) => {
        this.initNewBBByPeriod(bbConfig, candle, closeTimes.slice(0, index));
    });

    return candleData;
}

CustomBB.initNewBBByPeriod = function (bbConfig, candleData = {}, prevCloseTimes = []) {

    let period = !objectUtil.isEmpty(bbConfig["period"]) ? bbConfig["period"] : DEFAULT_PERIOD;
    let stdDev = !objectUtil.isEmpty(bbConfig["stdDev"]) ? bbConfig["stdDev"] : DEFAULT_STD_DEV;

    if (objectUtil.isEmpty(candleData["bb"])) {
        candleData["bb"] = {};
    }
    prevCloseTimes.push(candleData["close"]);

    // convert to number if needed
    prevCloseTimes = prevCloseTimes.map(Number);

    let bollBands = bollBand.calculate({period: period, values: prevCloseTimes, stdDev: stdDev});
    candleData["bb"] = (bollBands[bollBands.length - 1] !== undefined) ? bollBands[bollBands.length - 1] : {
        "middle": 0,
        "upper": 0,
        "lower": 0
    };
}

module.exports = CustomBB;