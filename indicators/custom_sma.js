const sma = require('technicalindicators').SMA;
const objectUtil = require('../utils/object_util');

const DEFAULT_VOLUME_PERIOD = 20;

/**
 * Calculate sma of price and volume
 */
CustomSMA = {};

CustomSMA.getSmaVolumeByPeriod = function (smaConfig, candleData = []) {
    let volumes = [];
    candleData.forEach(candle => {
        volumes.push(candle["volume"]);
    });

    //calculate sma
    candleData.forEach((candle, index) => {
        this.initNewSmaVolumeByPeriod(smaConfig, candle, volumes.slice(0, index));
    });

    return candleData;
}

CustomSMA.initNewSmaVolumeByPeriod = function (smaConfig, candleData = {}, prevVolumes = []) {

    let period = !objectUtil.isEmpty(smaConfig["period"]) ? smaConfig["period"] : DEFAULT_VOLUME_PERIOD;

    if (objectUtil.isEmpty(candleData["smaVolume"])) {
        candleData["smaVolume"] = {};
    }
    prevVolumes.push(candleData["volume"]);

    // convert to number if needed
    prevVolumes = prevVolumes.map(Number);

    let smaVolumes = sma.calculate({period: period, values: prevVolumes});
    candleData["smaVolume"] = (smaVolumes[smaVolumes.length - 1] !== undefined) ? smaVolumes[smaVolumes.length - 1] : 0;
}

module.exports = CustomSMA;