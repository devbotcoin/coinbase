const ema = require('technicalindicators').EMA;
const objectUtil = require('../utils/object_util');

const DEFAULT_PERIOD = 9;

const CustomEMA = {};

CustomEMA.getEMAByPeriods = function (emaConfig, candleData = []) {
    let periods = !objectUtil.isEmpty(emaConfig) ? emaConfig["periods"] : [];

    let closeTimes = [];
    candleData.forEach(candle => {
        //time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored
        closeTimes.push(candle["close"]);
    });
    //calculate ema by period
    candleData.forEach((candle, index) => {

        periods.forEach(period => {
            this.initNewEmaByPeriod(period, candle, closeTimes.slice(0, index));
        });
    });

    return candleData;
}

CustomEMA.initNewEmaByPeriod = function (period = DEFAULT_PERIOD, candleData = {}, prevCloseTimes = []) {
    if (objectUtil.isEmpty(candleData["emaPeriods"])) {
        candleData["emaPeriods"] = {};
    }
    prevCloseTimes.push(candleData["close"]);

    // convert to number if needed
    prevCloseTimes = prevCloseTimes.map(Number);

    let emas = ema.calculate({period: period, values: prevCloseTimes});
    candleData["emaPeriods"][period] = (emas[emas.length - 1] !== undefined) ? emas[emas.length - 1] : 0;
}


module.exports = CustomEMA;