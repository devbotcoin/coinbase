class PrevDay {
    constructor(exchangeApi) {
        this.exchangeApi = exchangeApi;
        this.symbolInfoInDays = [];

        this.initPrevDay();
    }

    initPrevDay() {
        let self = this;
        this.exchangeApi.getPrevDay(null, function (data) {
            data.forEach(symbolInfo => {
                let symbol = symbolInfo.s;
                self.symbolInfoInDays[symbol] = symbolInfo;
            })
        })
    }

    getSymbolInfoInDays() {
        return this.symbolInfoInDays;
    }


    getChange24HInfo(symbol) {
        let self = this;
        let symbolInfoInDay = this.symbolInfoInDays[symbol];
        let rate = 0;
        let symbolCap = symbolInfoInDay["q"];
        Object.keys(self.symbolInfoInDays).forEach(function (symbolInfo) {
            let symbolInfoInDay = self.symbolInfoInDays[symbolInfo];

            if ((symbol.endsWith("BTC") && symbolInfo.endsWith("BTC")) || (symbol.endsWith("USDT") && symbolInfo.endsWith("USDT"))) {
                if (Number(symbolInfoInDay["q"]) >= Number(symbolCap)) {
                    rate++;
                }
            }
        });

        return {
            "change": symbolInfoInDay["P"],
            "high": symbolInfoInDay["h"],
            "low": symbolInfoInDay["l"],
            "rate": rate
        }
    }
}

module.exports = PrevDay;
